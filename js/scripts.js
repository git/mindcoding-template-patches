function set_style(name){
	$('#stylesheet').attr("href", "/css/" + name + ".var");
	localStorage.setItem("theme", name);

	$('#yardi').attr('src', name == "slate" || name == "cyborg" ? "/img/yardi-white.png" : "/img/yardi-blue.png");
}

$( document ).ready(function() {
    var txt = $('#comments');
    hiddenDiv = $(document.createElement('div')),
    content = null;

    txt.addClass('txtstuff');
    hiddenDiv.addClass('hiddendiv common');

    $('body').append(hiddenDiv);

    $(".form-control").on('keyup', function () {
		content = $(this).val();

		content = content.replace(/\n/g, '<br>');
		hiddenDiv.html(content + '<br class="lbr">');

		$(this).css('height', hiddenDiv.height()+23);
	});

	$('#theme_slate'   ).on('click', function () { set_style("slate"); });
	$('#theme_cerulean').on('click', function () { set_style("cerulean"); });
	$('#theme_cyborg'  ).on('click', function () { set_style("cyborg"); });
	$('#theme_cosmo'   ).on('click', function () { set_style("cosmo"); });
});

var theme = localStorage.getItem("theme");
if(theme) {
	set_style(theme);
	$(document).ready(function(){
		$('#yardi').attr('src', theme == "slate" || theme == "cyborg" ? "/img/yardi-white.png" : "/img/yardi-blue.png");
	})
}

$( document ).ready(function() {
    var sb=$('#sidebar').detach();
    sb.appendTo($('#sponsors'));
    console.log( "inside!" );
});
